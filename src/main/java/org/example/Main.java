package org.example;

import au.com.bytecode.opencsv.CSVWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public class Main {
    private static final Logger log = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) throws IOException {
        ArrayList<InternalData> myList  = new ArrayList<>();

        InternalData d1 = new InternalData(LocalDate.now(),21, 13);
        InternalData d2 = new InternalData(LocalDate.of(2020,10,9), 22, 15);
        InternalData d3 = new InternalData(LocalDate.of(2020,10,8), 23, 16);

        myList.add(d1);
        myList.add(d2);
        myList.add(d3);

        String [] record1 = (d1.getDate().toString() +","+ d1.getTemperature() +","+ d1.getWetness()).split(",");
        String [] record2 = (d2.getDate().toString() +","+ d2.getTemperature() +","+ d2.getWetness()).split(",");
        String [] record3 = (d3.getDate().toString() +","+ d3.getTemperature() +","+ d3.getWetness()).split(",");

        Path csv = Files.createTempFile("data_",".csv");
        //String csv = "data_1.csv";
        System.out.println(csv);
        String zip = "D:\\ddd\\output.zip";

        try{
            CSVWriter writer = new CSVWriter(new FileWriter(String.valueOf(csv)));
            //Во время начала сохранения файла выводить в лог с уровнем INFO и маркером FILE
            //         информацию о том что начато создание файла. Указать имя файла
            log.info(Markers.FILE, "Starting write to file.");

            writer.writeNext(record1);
            writer.writeNext(record2);
            writer.writeNext(record3);
            writer.close();
        }
        catch (IOException ex){
            System.out.println(ex.getMessage());
            //При обработке IOException вывести в лог с уровнем ERROR и маркером FILE
            //         месседж исключения и его стектрейс
            log.error(Markers.FILE, "Error output zip to file.", ex);
        }

        try(ZipOutputStream zout = new ZipOutputStream(new FileOutputStream(zip));
            FileInputStream fis= new FileInputStream(String.valueOf(csv));)
        {
            ZipEntry entry1=new ZipEntry(String.valueOf(csv.getFileName()));
            zout.putNextEntry(entry1);
            byte[] buffer = new byte[fis.available()];
            fis.read(buffer);
            zout.write(buffer);
            zout.closeEntry();
            // Вывести в лог сообщение о том что процесс завершен. Упомянуть имя ZIP файла. Уровень DEBUG. Без маркера
            log.debug("Finish output zip to file.");
        }
        catch(Exception ex){
            System.out.println(ex.getMessage());
        }

        try(ZipInputStream zin = new ZipInputStream(new FileInputStream(zip)))
        {
            ZipEntry entry;
            String name;
            long size;
            while((entry=zin.getNextEntry())!=null){
                name = entry.getName(); // получим название i-го файла
                size=entry.getSize();  // получим размерв байтах
                //System.out.printf("File name: %s \t File size: %d \n", name, size);
                FileOutputStream fout = new FileOutputStream("D:\\ddd\\new" + name);
                for (int c = zin.read(); c != -1; c = zin.read()) {
                    fout.write(c);
                    // Вывести в лог прочитанные данные в удобном для чтения формате. Уровень INFO
                    log.info(String.valueOf(c));
                }
                fout.flush();
                zin.closeEntry();
                fout.close();
            }
        }
        catch(Exception ex) {
            System.out.println(ex.getMessage());
            //При обработке IOException вывести в лог с уровнем ERROR и маркером ARCHIVE
            //         месседж исключения и его стектрейс
            log.error(Markers.ARCHIVE, "Error output zip to file.", ex);
        }
    }
}

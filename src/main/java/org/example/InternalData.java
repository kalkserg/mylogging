package org.example;

import java.time.LocalDate;

/**
 * Данные которые хранятся в нашей системе
 */
public class InternalData {
    private LocalDate date;
    private int temperature;
    private int wetness;

    public InternalData(LocalDate date, int temperature, int wetness) {
        this.date = date;
        this.temperature = temperature;
        this.wetness = wetness;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public int getWetness() {
        return wetness;
    }

    public void setWetness(int wetness) {
        this.wetness = wetness;
    }
}
